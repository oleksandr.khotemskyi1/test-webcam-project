const { chromium } = require('playwright');

(async () => {
    const browser = await chromium.launch({
        // executablePath: '/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome',
        headless: true,
        defaultViewport: null,
        // ignoreDefaultArgs: true,
        args: [
            '--disable-dev-shm-usage',
            '--use-fake-device-for-media-stream',
            '--use-fake-ui-for-media-stream',
        ],
        permissions: [
            'camera'
        ],
    });
    const context = await browser.newContext({
    })
    await context.grantPermissions([
        'camera',
        'microphone'
    ]);
    const page = await context.newPage();
    await page.goto('https://webcamtests.com/');
    await page.waitForSelector('#webcam-launcher')
    await (await page.$('#webcam-launcher')).click()
    await page.waitForTimeout(20000)
    await page.screenshot({ path: `screenshot/result.png` });
    await browser.close()
})();